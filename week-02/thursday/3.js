// 3. A random color
// Output a random color from a given color
//Global
let colors = ["red", "green", "blue"];
const randomColor = (colors) => {
        let randomNumber = Math.floor(Math.random() * colors.length);
        return colors[randomNumber];
    }
    // console.log(randomColor(colors));