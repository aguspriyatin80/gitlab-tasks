// // 4. Split words without function .split(" ") and find the max
// var string = "Lorem ipsum is dummy text"
// j = 0;
// let word = []
// let cek1 = []
// let temp = ''

// x = 0;
// y = 0;
// z = 0;
// let kata = []
// let cek2 = []
// let tempo = ''

// //Using For
// function maxWordLength() {
//     for (let i = 0; i <= string.length; i++) {
//         if (string[i] != " ") {
//             temp += string[i]
//         } else {
//             word.push(temp);
//             temp = '';
//             cek1.push(word[j].length);
//             j++;
//         }
//         if (i == string.length - 1) {
//             word.push(temp);
//             cek1.push(word[j].length);
//         }
//     }
//     console.log(word);
//     //console.log(cek1);
//     //cek kata terpanjang
//     let max = Math.max(...cek1);
//     for (let i = 0; i <= cek1.length; i++) {
//         if (word[i].length == max) {
//             console.log(word[i]);
//             break;
//         }
//     }

//     //Using While
//     while (x < string.length) {
//         if (string[x] != " ") {
//             tempo += string[x]
//         } else {
//             kata.push(tempo);
//             tempo = '';
//             cek2.push(kata[y].length);
//             y++;
//         }
//         x++;
//         if (x == string.length) {
//             kata.push(tempo);
//             cek2.push(kata[y].length);
//         }
//     }
//     console.log(kata);
//     //console.log(cek2);
//     let maks = Math.max(...cek2);
//     while (z <= cek2.length) {
//         if (kata[z].length == maks) {
//             console.log(kata[z]);
//             break;
//         }
//         z++;
//     }
// }

// //Test Case
// maxWordLength(string); //Lorem

let string = "Lorem ipsum is dummy text";
const splitWords = (string) => {
    let temp = [];
    let word = "";

    for (let i = 0; i < string.length; i++) {
        if (string[i] !== " ") {
            word += string[i];
            // console.log(word);
        } else {
            // console.log(word)
            temp.push(word);
            word = "";
        }

        if (i === string.length - 1) {
            temp.push(word);
        }
    }
    return temp
}

console.log(splitWords(string));