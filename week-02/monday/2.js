// 2. Print the n first numbers

for (var angka = 1; angka <= 15; angka++) {
    if (angka % 3 == 0 && angka % 5 == 0) {
        console.log(angka + " Kelipatan 3 dan 5");
    } else if (angka % 5 == 0) {
        console.log(angka + " Kelipatan 5");
    } else if (angka % 3 == 0) {
        console.log(angka + " Kelipatan 3");
    } else {
        console.log(angka);
    }
}