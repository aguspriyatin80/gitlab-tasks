// 1. Check old or even number
// Test Case
// Input 5 -> Output Odd
// Input 10 -> Output Even

var angka = 5;
if (angka % 2 == 0) {
    console.log("Even")
} else {
    console.log("Odd")
}

var angka = 10;
if (angka % 2 == 0) {
    console.log("Even")
} else {
    console.log("Odd")
}