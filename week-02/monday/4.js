// 4. Split words without function .split(" ")

var string = "Lorem ipsum is dummy text";

// Using For

// Using While

// Ouput
// ["Lorem","ipsum","is","dummy","text"]

// Menggunakan For

var string = "Lorem ipsum is dummy text";
var kata = []
var katakan = '';

for (let i = 0; i <= string.length; i++) {
    katakan += string[i];
    if (string[i] === " ") {
        kata.push(katakan.trim());
        katakan = "";
    }
    if (i === string.length - 1) {
        kata.push(katakan);
    }
}

console.log(kata);


// Menggunakan while
var kata = []
var katakan = ""
var i = 0;

while (i < string.length) {
    katakan += string[i]
    i++;
    if (string[i] == " ") {
        kata.push(katakan.trim());
        katakan = "";
    }
    if (i == string.length) {
        kata.push(katakan.trim());
    }
}
console.log(kata)


// // Jika menggunakan .split

// var explode = string.split(" ");
// console.log(explode);