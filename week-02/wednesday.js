// var container = document.querySelector('.container');
// console.log(container);

// var input = document.querySelector('input');
// console.log(input);

// var taskData = document.querySelector('#task-data');
// console.log(taskData);

// var btn = document.getElementById('btn');
// btn.addEventListener('click',submitHandler);
var tasks = [{
        id: 1,
        title: "Makan",
        status: "progress"
    },
    {
        id: 2,
        title: "Minum",
        status: "done"
    }
];

getData();

function submitHandler() {
    // console.log("Click already worked.");
    var taskValue = document.getElementById('task-title').value;
    var taskStatus = document.getElementById('task-status').value;

    addData(taskValue, taskStatus);
    getData();
    document.getElementById('task-title').value = "";

    return false;
}

function getData() {
    var taskData = document.getElementById('task-data');
    taskData.innerHTML = "";

    for (let i = 0; i < tasks.length; i++) {
        let taskHTML = `
            <p>${tasks[i].id}. ${tasks[i].title} , status : ${tasks[i].status}</p>
        `

        taskData.insertAdjacentHTML('beforeend', taskHTML);
    }

}

function addData(title, status) {
    var taskObject = createObject(title, status);
    tasks.push(taskObject);
    console.log(`"${title}" has been inserted!`);

}

function createObject(title, status) {
    var temp = {
        id: generateId() + 1,
        title: title,
        status: status
    }
    return temp
}

function generateId() {
    var tasksLength = tasks.length;
    var id = tasks[tasksLength - 1].id;
    return id
        // return tasks[tasks.length - 1].id
}

//DOM Exercise
//Background
//Mr. James decided to make a notes about his Income and Expenses every month.

//Main Task
//Make Income and Expenses Apps

//Task 1 : Prepare variable
//Prepare global variable for data
var posts = [];
//Contains object
//Example
// var posts = [
//     {
//         id : 1,
//         title : "Salary Income",
//         type : "income",
//         total : 5000000
//     }
// ]


//Task 2 : getData
//Code getData function to read the data from the global variable
function getData() {
    //Code here
}


//Task 3 : addData
//User can input the data
function addData(post) {
    //Code here
}


//Task 4 : deleteData
//User can delete post
function deleteData(post) {

}


//Task 5 : Split the files
//Developer can split the files (module.exports and require)