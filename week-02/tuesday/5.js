// 5. Encrypt the string
// Rules : swap the letter, after the letter itself.
// Example : "wow" -> //xpx
// after letter w is x, after letter o is p, and so on.
function swap(string) {
    //code here
    var temp = '';
    var a = 'abcdefghijklmnopqrstuvwxyz';
    var b = 'bcdefghijklmnopqrstuvwxyza';
    for (let i = 0; i < string.length; i++) {
        for (let j = 0; j < a.length; j++) {
            if (string[i] === a[j]) {
                temp += b[j];
            }
        }
    }
    console.log(temp);
}

//Test
swap("wow") //xpx
swap("javascript") //kbwbtdsjqu