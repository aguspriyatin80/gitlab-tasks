// 1. Find the Faktor Persekutuan terbesar
function fpb(number1, number2) {
    //code here
    //faktor number1
    var a = [];
    var b = [];
    for (let i = 1; i <= number1; i++) {
        if (number1 % i === 0) {
            a.push(i);
        } else {
            continue;
        }

    }
    //console.log(a);
    //faktor number2
    for (let i = 1; i <= number2; i++) {
        if (number2 % i === 0) {
            b.push(i);
        } else {
            continue;
        }

    }
    //console.log(b);
    var minNumber;
    if (number1 < number2) {
        minNumber = number1;
    } else {
        minNumber = number2;
    }
    for (let i = minNumber; i >= 1; i--) {
        if (number1 % i === 0 && number2 % i === 0) {
            return i;
        }
    }
}

//Test
console.log(fpb(30, 50)) //10
console.log(fpb(12, 15)) //3
console.log(fpb(17, 35)) //1