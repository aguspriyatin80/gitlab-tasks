//4. Check if 'x' and 'o' are the same  
function checkXO(string) {
    //code here
    var siX = [];
    var siO = [];
    //console.log(string.length);
    for (let i = 0; i < string.length; i++) {
        //console.log(string[i]);
        if (string[i] === 'x') {
            siX.push(string[i]);
        } else {
            siO.push(string[i]);
        }
    }
    if (siX.length === siO.length) {
        return '1';
    } else {
        return '-1';
    }

    //console.log(siX);
    //console.log(siO);
}

//Test
console.log(checkXO('xxxxxooooo')) //1
console.log(checkXO('xxxooooo')) //-1