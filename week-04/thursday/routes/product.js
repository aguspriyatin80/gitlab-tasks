const { Router } = require('express');
const ProductRoutes = Router();

ProductRoutes.get('/', (req, res) => {
    //res.send("Ini product Home")
    res.render('product.ejs');
})

ProductRoutes.get('/tambah', (req, res) => {
    res.send("Ini product add")
})

module.exports = ProductRoutes;