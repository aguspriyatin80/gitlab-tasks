const { Router } = require('express');
const router = Router();

const ProductRouters = require('./product')
router.get ('/', (req, res) =>{
    res.render('index.ejs');
})
router.use ('/product', ProductRouters);

module.exports = router;