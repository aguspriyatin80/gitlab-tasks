const fs = require('fs');

class Todo {
    constructor(id, task, status, tag, created_at, complete_at) {
        this.id = id;
        this.task = task;
        this.status = status;
        this.tag = tag;
        this.created_at = created_at;
        this.complete_at = complete_at;
    }

static help() {
    const toHelp = `
    node todo.js
    node todo.js toHelp
    node todo.js list  
    node todo.js add <task>
    node todo.js update <id> <task>
    node todo.js delete <id>
    node todo.js complete <id>
    node todo.js uncomplete <id>
    `
    console.log(toHelp);
    return toHelp;
}

static list(params) {
    const data = fs.readFileSync('./data.json', 'utf8');
    const parseData = JSON.parse(data);

    let tempData = [];
    parseData.forEach(data => {
        const { id, task, status, tag, created_at, complete_at} = data;
        tempData.push(new Todo(id, task, status, tag, new Date(created_at), complete_at))
    });
    // console.log("Berenang");
    return tempData;
}

static add(params) {
    const todo = this.list();
    const [ task, status, tag] = params;

    const nextId = todo[todo.length - 1].id + 1;
    const tempTodo = {
        id: nextId,
        task : task,
        status : (status === 'true'),
        tag : tag,
        created_at : new Date(),
        complete_at : new Date()
    }
    todo.push(tempTodo);

    this.save(todo);
    return `${task} begin..`
}

static update(params) {
    const todo = this.list();
    const id = Number(params[0]);
    const task = params[1];

    todo.forEach(todo => {
        if(todo.id === id) {
            todo.task = task;
        }
    });
    this.save(todo);
    return `${task} updated`;
}

static delete(params) {
    const todo = this.list();
    const id = Number(params[0]);

    const tempData = todo.filter((todo) => todo.id !== id);

    this.save(tempData);
    return `Number ${id} deleted`
}

static complete(params) {
    const todo = this.list();
    todo.forEach(todo => {
        if(todo.id === Number(params)) {
            todo.status = true;
            todo.uncomplete_at = new Date();
        }
    })
    this.save(todo);
    return `Voilaa Done`
}

static uncomplete(params) {
    const todo = this.list();
    todo.forEach(todo => {
        if(todo.id === Number(params)) {
            todo.status = false;
            todo.uncomplete_at = null;
        }
    })
    this.save(todo);
    return `Yahhh failed!`
}

static save(data){
    fs.writeFileSync('./data.json', JSON.stringify(data, null,2));
}

}

module.exports = Todo;