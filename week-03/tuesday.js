// 4Pillars of OOP
/**
 * Inheritance
 * Polymorphism : Overriding and Overload
 * Encapsulation : Access Modifier -> Public, Protected, Private. Setter and Getter
 * Abstraction
 */

/**
 * Seorang farmer memiliki sebuah Kebun. Dalam kebun tersebut terdapat banyak pohon buah. Seperti Apple, Pear, Watermelon. dsb
 * Setiap pohon tersebut dapat Bertumbuh, Menghasilkan buah dalam usia matang nya, dan berhenti untuk di Budidayakan pada umur tertentu.
 * 
 * Buat sebuah class FruitTree, yang memiliki attribute :
 * - type 
 * - age
 * - fruits
 * - totalFruits
 * - matureAge -> pohon akan menghasilkan buah
 * - stopProducing 
 * Dan memiliki method : 
 * - growUp()
 * - produceFruits()
 * - status()
 */

class FruitTree {
    constructor(type, age, fruits, totalFruits, matureAge, stopProducing) {
            this._type = type;
            this._age = age;
            this._fruits = fruits || 0;
            this._totalFruits = totalFruits || 0;
            this._matureAge = matureAge;
            this._stopProducing = stopProducing;
        }
        //Getter
    get type() {
        return this._type;
    }
    get age() {
        return this._age;
    }
    get fruits() {
        return this._fruits;
    }
    get totalFruits() {
        return this._totalFruits;
    }
    get matureAge() {
        return this._matureAge;
    }
    get stopProducing() {
            return this._stopProducing;
        }
        //Setter
    set setType(type) {
        this._type = type;
        // console.log("Type has been set! Thanks.")
    }
    set setAge(age) {
        this._age = age;
    }
    set setFruits(fruits) {
        this._fruits = fruits;
    }
    set setTotalFruits(fruits) {
        this._totalFruits = fruits;
    }
    set setMatureAge(matureAge) {
        this._matureAge = matureAge;
    }
    set setStopProducing(age) {
        this._stopProducing = age
    }

    growUp() {
        let randomNumber = Math.ceil(Math.random() * 3)
        this.setAge = this._age += randomNumber;
    }
    produceFruits() {
        let fruitLimit = 15;
        let fruitProduced = Math.ceil(Math.random() * fruitLimit)

        if (this.age >= this.matureAge && this.age <= this.stopProducing) {
            // console.log("Sudah matang");
            this.setFruits = fruitProduced;
            this.setTotalFruits = this.totalFruits + fruitProduced;
        }
    }
    status() {
        if (this.age < this.stopProducing) {
            console.log(`${this.type} tree is ${this.age} years old. Produced ${this.fruits} fruits.`)
        } else {
            console.log(`${this.type} tree has produced total ${this.totalFruits} fruits.`)
        }
    }
}

class AppleTree extends FruitTree {
    constructor() {
        super("Apple", 1, 0, 0, 5, 15);
    }
}
class PearTree extends FruitTree {
    constructor() {
        super("Pear", 2, 1, 3, 5, 10)
    }
}
class WatermelonTree extends FruitTree {
    constructor(type, age, fruits, totalFruits, matureAge, stopProducing) {
        super(type, age, fruits, totalFruits, matureAge, stopProducing)
    }
}
let pohonApel = new AppleTree();
let pohonPir = new PearTree();
let pohonSemangka = new WatermelonTree("Watermelon", 1, 0, 0, 5, 10);

console.log("===== APPLE TREE ======");
while (pohonApel.age < pohonApel.stopProducing) {
    pohonApel.growUp();
    pohonApel.produceFruits();
    pohonApel.status();
}

console.log("===== PEAR TREE ======");
while (pohonPir.age < pohonPir.stopProducing) {
    pohonPir.growUp();
    pohonPir.produceFruits();
    pohonPir.status();
}

//Factory Class
class Garden {
    constructor(field) {
            this._field = field || [];
        }
        //Getter
    get field() {
        return this._field;
    }
    set setField(field) {
            this._field = field;
        }
        //Setter
    insertTree(tree) {
        this._field.push(tree);
    }
    report() {
        console.log("===== JACK'S GARDEN =====")
            // console.log(this.field);
        this.field.forEach(el => {
            console.log(`${el.type} tree has produced total ${el.totalFruits} fruits.`)
        })
    }
}
let garden = new Garden();
garden.insertTree(pohonApel);
garden.insertTree(pohonPir);
garden.report();