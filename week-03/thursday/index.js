const Armymodul = require("./army.js");
const Knight = Armymodul.Knight;
const Spear = Armymodul.Spear;
const Archer = Armymodul.Archer;
const Barack = require("./barack.js");


// Input Army Here //


//Input Knight Army//
let Knight1 = new Knight();
Knight1.name = "ryu";
Knight1.talkKnight();
Knight1.training(2); // random leveling
Knight1.training(2); // random leveling
Knight1.talkKnight();



let Knight2 = new Knight();
Knight2.name = "Lubu";
Knight2.talkKnight();
Knight2.training(3); // random leveling
Knight2.training(1); // random leveling
Knight2.talkKnight();

//Input Spear Army//
let Spear1 = new Spear();
Spear1.name = "Zhou yun";
Spear1.talkSpear();
Spear1.training(4); // random leveling
Spear1.training(2); // random leveling
Spear1.talkSpear();

let Spear2 = new Spear();
Spear2.name = "Zhilong";
Spear2.talkSpear();
Spear2.training(1); // random leveling
Spear2.training(4); // random leveling
Spear2.talkSpear();


// Input Archer Army//
let Archer1 = new Archer();
Archer1.name = "Robinhood";
Archer1.talkArcher();
Archer1.training(1); // random leveling
Archer1.training(1); // random leveling
Archer1.talkArcher();

let Archer2 = new Archer();
Archer2.name = "Scout";
Archer2.talkArcher();
Archer2.training(2); // random leveling
Archer2.training(2); // random leveling
Archer2.talkArcher();



let barack = new Barack();
//recruit army to barack
barack.recruit(Knight1);
barack.recruit(Knight2);
barack.recruit(Spear1);
barack.recruit(Spear2);
barack.recruit(Archer1);
barack.recruit(Archer1);

//remove army from group
barack.disband("Zhilong")
barack.summon();

//grouping army then how much enemies
barack.groupingteam();
barack.summon();