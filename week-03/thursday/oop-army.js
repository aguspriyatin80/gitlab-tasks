class Army {
    constructor(name, type, level) {
        this.name = name;
        this.type = type;
        this.level = level || 1;
    }

    //method
    talk() {
        console.log(`Hi! my name is ${this.name}. I am a/an ${this.type} and now I am at level ${this.level}`);
    }

    training(farming) {
        let levelLimit = 10;
        for (let i = 1; i <= farming; i++) {
            if (this.level < levelLimit) {
                this.level += 1;
                console.log(`level up to ${this.level} !!`);
            } else {
                console.log(`level ${this.level}, max level have reached!`);
            }
        }
    }
}

class Knight extends Army {
    constructor(name) {
        super(name, 'Knight', 1);
        this.name = name;
    }

    talkKnight() { //override the talk method
        super.talk();
        console.log(`I am part of Agrippa's Armies`);
    }
}

class Spearman extends Army {
    constructor(name) {
        super(name, 'Spearman', 1);
        this.name = name;
    }

    talkSpearman() { //override the talk method
        super.talk();
        console.log(`I am part of Agrippa's Armies`);
    }
}

class Archer extends Army {
    constructor(name) {
        super(name, 'Archer', 1);
        this.name = name;
    }

    talkArcher() { //override the talk method
        super.talk();
        console.log(`I am part of Agrippa's Armies`);
    }
}

//vvvv PLAY THE CODE BELOW HERE vvvv
//====================================

let Demon = new Knight();
Demon.name = 'Demon';
Demon.talkKnight();
Demon.training(7); //input total training given to Demon
Demon.talkKnight();


let Beelzebub = new Knight();
Beelzebub.name = 'Beelzebub';
Beelzebub.talkKnight();
Beelzebub.training(5); //input total training given to Beelzebub
Beelzebub.talkKnight();


let Satan = new Spearman();
Satan.name = 'Satan';
Satan.talkSpearman();
Satan.training(2); //input total training given to Satan
Satan.talkSpearman();


let Lucifer = new Spearman();
Lucifer.name = 'Lucifer';
Lucifer.talkSpearman();
Lucifer.training(9); //input total training given to Lucifer
Lucifer.talkSpearman();

let DemonLord = new Archer();
DemonLord.name = 'Demon Lord';
DemonLord.talkArcher();
DemonLord.training(10); //input total training given to DemonLord
DemonLord.talkArcher();

let Ogre = new Archer();
Ogre.name = 'Ogre';
Ogre.talkArcher(); //Ogre doesn't want to train

let Oga = new Knight();
Oga.name = 'Oga';
Oga.talkKnight();
Oga.training(12); //input total training given to Oga
Oga.talkKnight();

//====================================
//^^^^ PLAY THE CODE UP HERE ^^^^


class Barrack {
    constructor(slots, groups) {
        this.slots = slots || [];
        this.groups = groups;
    }
    recruit(army) {
        this.slots.push(army);
    }
    summon() {
        console.log("!!==vvv== Agrippa's Armies Lists ==vvv==!!");
        console.log(this.slots);

        console.log(`Type: Knight, Total: ${this.groups.Knight.length} member.`);
        this.groups.Knight.forEach(el => {
            console.log(`Name: ${el.name}, level : ${el.level}.`);
        })
        console.log('--------------------------------');

        console.log(`Type: Spearman, Total: ${this.groups.Spearman.length} member.`);
        this.groups.Spearman.forEach(el => {
            console.log(`Name: ${el.name}, level : ${el.level}.`);
        })
        console.log('--------------------------------');

        console.log(`Type: Archer, Total: ${this.groups.Archer.length} member.`);
        this.groups.Archer.forEach(el => {
            console.log(`Name: ${el.name}, level : ${el.level}.`);
        })
        console.log('--------------------------------');
        console.log(`Agrippa has total ${this.slots.length} armies in Barrack.`)

        // console.log('Type: Knight');
        // this.slots.forEach(el => {
        //     if(el.type === 'Knight'){
        //         console.log(`Name: ${el.name}, level : ${el.level}.`);
        //         count+=1;
        //     }
        // })
        // console.log(`Total ${count} Knights in Barrack.`);
        // count = 0;
        // console.log('--------------------------------');

        // console.log('Type: Spearman');
        // this.slots.forEach(el => {
        //     if(el.type === 'Spearman'){
        //         console.log(`Name: ${el.name}, level : ${el.level}.`);
        //         count+=1;
        //     }
        // })
        // console.log(`Total ${count} Spearman in Barrack.`);
        // count = 0;
        // console.log('--------------------------------');

        // console.log('Type: Archer');
        // this.slots.forEach(el => {
        //     if(el.type === 'Archer'){
        //         console.log(`Name: ${el.name}, level : ${el.level}.`);
        //         count+=1;
        //     }
        // })
        // console.log(`Total ${count} Archer in Barrack.`);
        // count = 0;
        // console.log('--------------------------------');

        // console.log(`Agrippa has total ${this.slots.length} armies in Barrack.`)

    }

    grouping() {
        let tempGroup = {
            Knight: [

            ],
            Spearman: [

            ],
            Archer: [

            ]
        };
        this.slots.forEach(el => {
            if (el.type === 'Knight') {
                tempGroup.Knight.push(el);
            } else if (el.type === 'Spearman') {
                tempGroup.Spearman.push(el);
            } else {
                tempGroup.Archer.push(el);
            }
        })
        this.groups = tempGroup;
        console.log(this.groups);
    }

    disband(name) {
        for (let i = 0; i < this.slots.length; i++) {
            if (this.slots[i] === name) {
                this.slots.splice(i, 1);
            }
        }
        //console.log(this.slots);
    }

}

//CHECK THE BARRACK BELOW HERE 


let barrack = new Barrack();
barrack.recruit(Demon);
barrack.recruit(Beelzebub);
barrack.recruit(Satan);
barrack.recruit(Lucifer);
barrack.recruit(DemonLord);
barrack.recruit(Ogre);
barrack.recruit(Oga);

barrack.grouping();
barrack.summon();

barrack.disband(Ogre);
barrack.grouping();
barrack.summon();