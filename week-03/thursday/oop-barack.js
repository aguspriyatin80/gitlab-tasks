class Barack {
    constructor(slots, group) {
        this.slots = slots || [];
        this.group = group;
    }

    recruit(army) {
        this.slots.push(army);
    }

    summon() {
        console.log(`List Enemies ${this.slots}`);
    }

    disband(name) {
        for (let i = 0; i <= this.slots.length; i++) {
            if (this.slots[i] === name) {
                this.slots.splice(i, 1);
            } else {
                continue;
            }
        }
        //console.log(remove);
    }
    groupingteam() {
        let grouping = {
            Knight: [],
            Spear: [],
            Archer: []
        }
        this.slots.forEach(el => {
            if (el.type === "Knight") {
                grouping.Knight.push(el);
            } else if (el.type === "Spear") {
                grouping.Spear.push(el);
            } else {
                grouping.Archer.push(el);
            }
        })
        this.group = grouping;
        console.log(this.group);
    }

}
module.exports = Barack;