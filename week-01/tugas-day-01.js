/*

MEMBUAT SUMMARY OF LEARNING TODAY

1. Hari ini belajar tentang :
   - sistem operasi boleh menggunakan windows/linux/mac
   - Pengenalan path learning of javascript
   - instalasi git terlebih dahulu
   - perbedaan backend dan frontend
     backend:  mengurusi logic dari sisi server agar website dapat diakses oleh user secara dinamis,
               dengan menggunakan bahasa pemrograman seperti PHP, Nodejs maupun framework nya.
     frontend: mengurusi tampilan website yang hasilnya dapat dilihat langsung oleh user, 
               dengan menggunakan HTML, CSS, Javascript maupun framework nya.

2. Sesi pertama ini saya sudah dapat :
   - install visual studio code
   - dasar menggunakan visual studio code
   - mengetik script html dan javascript dan menampilkan hasilnya di browser

*/