/*

MEMBUAT SUMMARY OF LEARNING TODAY

- review pertemuan ke-1 (day-01)
  1) menjelaskan perbedaan frontend dan backend
  2) mengenal API
  3) sudah install git
  4) sudah instal visual studio code dan dasar penggunaannya
  
- apa itu git?
  git adalah salah satu version control system yang digunakan untuk 
  memantau semua perubahan yang terjadi pada repository file proyek,
  baik dikerjakan sendiri maupun bersama tim.

- beberapa command git yang sudah dipelajari, antara lain:
  1) git init
     fungsinya untuk menjadikan folder saat ini sebagai repository lokal
  2) git remote add origin
     fungsinya untuk menghubungkan repository lokal 
     dengan repository yang ada di server (remote)
  3) git add
     fungsinya untuk menambahkan file tertentu yang mengalami 
     perubahan ke dalam repository lokal
  4) git commit -m "<message name>"
     fungsinya untuk menyimpan perubahan ke dalam repository lokal 
     dengan disertai pesan deskripsi
  5) git push origin master
     fungsinya untuk menyimpan semua perubahan yang ada di repository lokal
     dan mengupdate perubahannya ke dalam repository remote, sehingga 
     isi repository lokal dengan remote sama saja.

*/