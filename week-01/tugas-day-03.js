/*

MEMBUAT SUMMARY OF LEARNING TODAY:

1) review pertemuan sebelumnya (day 2) yang membahas dasar penggunaan git dan perintah-perintah yang sering digunakan
2) membahas perintah git yang baru, yaitu:
   git checkout -b <branch_name> : fungsinya untuk membuat branch baru sekaligus pindah posisi aktif ke branch tersebut
   git pull origin <branch_name> : fungsinya untuk memperbaharui repository lokal ke komit terkini yang ada di remote branch tersebut
3) belajar html dan css dasar
4) membuat  halaman web sederhana menggunakan html dan css
5) menerapkan git ketika kolaborasi tim dalam menulis kode untuk membuat website 
   (dibagi menjadi 4 kelompok, kelompok 1 terdiri dari 3 orang, dan kelompok 2,3,4 masing-masing 2 orang)

*/